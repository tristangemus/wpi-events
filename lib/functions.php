<?php

/**
 * Display a formatted date
 *
 * @param  integer  $post_id Event ID
 * @param  boolean $echo     Echo or return the result
 *
 * @return string/void       Formatted event string or void
 */
function wpi_event_date( $post_id, $echo = true ) {
	$start_date = get_post_meta( $post_id, 'event-start-date', true );
	$end_date = get_post_meta( $post_id, 'event-end-date', true );

	// Only display the full date once if they are the same day
	if ( date( 'FdY', $start_date ) === date( 'FdY', $end_date ) ) {
		$formatted_date = sprintf(
			'%s %s - %s',
			date( 'l, F j, Y', $start_date ),
			date( 'g:i A', $start_date ),
			date( 'g:i A', $end_date )
		);
	} else {
		$formatted_date = sprintf(
			'%s - %s',
			date( 'l, F j, Y g:i A', $start_date ),
			date( 'l, F j, Y g:i A', $end_date )
		);
	}

	if ( $echo ) {
		echo $formatted_date;
	} else {
		return $formatted_date;
	}
}
