<?php

class WPI_Events_Post_Type_Event {

	/**
	 * Post type rewrite slug
	 */
	const SLUG = 'event';

	/**
	 * Post type name registered in Wordpress
	 */
	const NAME = 'wpi-event';

	/**
	 * Post type singular name
	 */
	const NAME_SINGULAR = 'Event';

	/**
	 * Post type plural name
	 */
	const NAME_PLURAL = 'Events';

	/**
	 * Post type lowercase name
	 */
	const NAME_LOWER = 'event';

	/**
	 * Post type lowercase plural name
	 */
	const NAME_LOWER_PLURAL = 'events';

	/**
	 * Query variable used to display validation message
	 */
	const VALIDATION_QUERY_VAR = 'event_validation_message';

	/**
	 * Used to store the desired validation message key
	 *
	 * @var string
	 */
	protected $validation_message_key = '';

	/**
	 * Array of validation messages
	 *
	 * @var array
	 */
	protected $validation_messages = [
		'date_order' => 'End date must be after the start date.'
	];

	/**
	 * Init hooks & filters
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register' ] );
		add_action( 'add_meta_boxes_' . self::NAME, [ $this, 'add_event_details_meta_box' ] );
		add_action( 'save_post_' . self::NAME, [ $this, 'event_details_meta_box_save' ] );
		add_action( 'admin_notices', [ $this, 'validation_error' ] );
		add_action( 'pre_get_posts', [ $this, 'archive_query' ] );
		add_filter( 'template_include', [ $this, 'archive_template' ] );
	}

	/**
	 * Register post type
	 */
	public function register() {
		$labels = [
			'name'                  => _x( self::NAME_PLURAL, 'Post Type General Name', WPI_EVENTS_LOCALE ),
			'singular_name'         => _x( self::NAME_SINGULAR, 'Post Type Singular Name', WPI_EVENTS_LOCALE ),
			'menu_name'             => __( self::NAME_PLURAL, WPI_EVENTS_LOCALE ),
			'name_admin_bar'        => __( self::NAME_PLURAL, WPI_EVENTS_LOCALE ),
			'archives'              => __( self::NAME_SINGULAR . ' Archies', WPI_EVENTS_LOCALE ),
			'attributes'            => __( self::NAME_SINGULAR . ' Attributes', WPI_EVENTS_LOCALE ),
			'parent_item_colon'     => __( 'Parent ' . self::NAME_SINGULAR . ':', WPI_EVENTS_LOCALE ),
			'all_items'             => __( 'All ' . self::NAME_PLURAL, WPI_EVENTS_LOCALE ),
			'add_new_item'          => __( 'Add New ' . self::NAME_SINGULAR, WPI_EVENTS_LOCALE ),
			'add_new'               => __( 'Add New', WPI_EVENTS_LOCALE ),
			'new_item'              => __( 'New ' . self::NAME_SINGULAR, WPI_EVENTS_LOCALE ),
			'edit_item'             => __( 'Edit ' . self::NAME_SINGULAR, WPI_EVENTS_LOCALE ),
			'update_item'           => __( 'Update ' . self::NAME_SINGULAR, WPI_EVENTS_LOCALE ),
			'view_item'             => __( 'View ' . self::NAME_SINGULAR, WPI_EVENTS_LOCALE ),
			'view_items'            => __( 'View ' . self::NAME_PLURAL, WPI_EVENTS_LOCALE ),
			'search_items'          => __( 'Search ' . self::NAME_SINGULAR, WPI_EVENTS_LOCALE ),
			'not_found'             => __( 'Not found', WPI_EVENTS_LOCALE ),
			'not_found_in_trash'    => __( 'Not found in Trash', WPI_EVENTS_LOCALE ),
			'featured_image'        => __( 'Featured Image', WPI_EVENTS_LOCALE ),
			'set_featured_image'    => __( 'Set featured image', WPI_EVENTS_LOCALE ),
			'remove_featured_image' => __( 'Remove featured image', WPI_EVENTS_LOCALE ),
			'use_featured_image'    => __( 'Use as featured image', WPI_EVENTS_LOCALE ),
			'insert_into_item'      => __( 'Insert into ' . self::NAME_LOWER, WPI_EVENTS_LOCALE ),
			'uploaded_to_this_item' => __( 'Uploaded to this ' . self::NAME_LOWER, WPI_EVENTS_LOCALE ),
			'items_list'            => __( self::NAME_PLURAL . ' list', WPI_EVENTS_LOCALE ),
			'items_list_navigation' => __( self::NAME_PLURAL . ' list navigation', WPI_EVENTS_LOCALE ),
			'filter_items_list'     => __( 'Filter ' . self::NAME_LOWER_PLURAL . ' list', WPI_EVENTS_LOCALE )
		];

		$args = [
			'label'                 => __( self::NAME_SINGULAR, WPI_EVENTS_LOCALE ),
			'description'           => __( 'Events that we are managing', WPI_EVENTS_LOCALE ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'revisions', 'thumbnail' ],
			'taxonomies'            => [ 'event-category' ],
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'menu_icon' 			=> 'dashicons-calendar',
			'rewrite'				=> [
				'rewrite' => self::SLUG,
				'with_front' => false
			]
		];

		register_post_type( self::NAME, $args );
	}

	/**
	 * Register event details meta box
	 */
	public function add_event_details_meta_box() {
		add_meta_box(
			'event_details_meta_box',
			__( 'Event Details', WPI_EVENTS_LOCALE ),
			[ $this, 'event_details_meta_box_callback' ],
			self::NAME,
			'normal',
			'low'
		);
	}

	/**
	 * Render event details meta box
	 *
	 * @param  WP_POST $post WP Post Object
	 */
	public function event_details_meta_box_callback( $post ) {
		wp_nonce_field( basename( __FILE__ ), 'event_details_meta_box_nonce' );

		$start_date = $this->build_date_array( get_post_meta( $post->ID, 'event-start-date', true ) );
		$end_date = $this->build_date_array( get_post_meta( $post->ID, 'event-end-date', true ) );

		include WPI_EVENTS_ROOT . '/views/event-details-meta-box.php';
	}

	/**
	 * Save event details meta box
	 *
	 * @param  int $post_id Post ID
	 */
	public function event_details_meta_box_save( $post_id ) {
		if ( ! isset( $_POST['event_details_meta_box_nonce'] ) ||
			 ! wp_verify_nonce( $_POST['event_details_meta_box_nonce'], basename( __FILE__ ) ) ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		$start_date = $this->build_timestamp( 'start', $_POST );
		$end_date = $this->build_timestamp( 'end', $_POST );

		$data = [
			'start_date' => $start_date,
			'end_date' => $end_date
		];

		if ( $this->validation_message_key = $this->validate_post_meta( $data ) ) {
			add_filter( 'redirect_post_location', [ $this, 'add_notice_query_var' ], 10, 1 );

			return false;
		}

		if ( isset( $_POST['event-start-date'] ) ) {
			update_post_meta( $post_id, 'event-start-date', $start_date );
		}

		if ( isset( $_POST['event-end-date'] ) ) {
			update_post_meta( $post_id, 'event-end-date', $end_date );
		}
	}

	/**
	 * Display a validation error if applicable
	 */
	public function validation_error() {
		if ( isset( $_GET[ self::VALIDATION_QUERY_VAR ] ) ) {
			printf(
				'<div class="notice notice-error is-dismissible"><p>%s</p></div>',
				$this->validation_messages[ $_GET[ self::VALIDATION_QUERY_VAR ] ]
			);
		}
	}

	/**
	 * Add the validation query variable to the request
	 *
	 * @param string $location Filtered destination URL
	 */
	public function add_notice_query_var( $location ) {
		return add_query_arg( array( self::VALIDATION_QUERY_VAR => $this->validation_message_key ), $location );
	}

	/**
	 * Display the archive template
	 *
	 * @param  string $template Template path
	 *
	 * @return string           Filtered template path
	 */
	public function archive_template( $template ) {
		if ( is_post_type_archive( self::NAME ) ) {
			$template = WPI_EVENTS_ROOT . '/views/archive.php';
		}

		return $template;
	}

	/**
	 * Order the event archive results
	 *
	 * @param  WP_Query &$query The WP_Query instance (passed by reference)
	 *
	 * @return WP_Query         The WP_Query instance (passed by reference)
	 */
	public function archive_query( &$query ) {
		$query->set( 'meta_key', 'event-start-date' );
		$query->set( 'orderby', 'meta_value_num' );
		$query->set( 'order', 'ASC' );
	}

	/**
	 * Validate specified post meta
	 *
	 * @param  array $data Array of meta values
	 */
	private function validate_post_meta( $data ) {
		// End date must be after start date
		if ( $data['end_date'] < $data['start_date'] ) {
			return 'date_order';
		}
	}

	/**
	 * Builds an array of specific date formats
	 *
	 * @param  integer $date Unix timestamp
	 *
	 * @return array         Array of dates/times in specified formats
	 */
	private function build_date_array( $date ) {
		if ( $date && ! empty( $date ) ) {
			return [
				'date' => date( 'F j, Y', $date ),
				'time' => date( 'g:i A', $date )
			];
		}

		return [
			'date' => '',
			'time' => ''
		];
	}

	/**
	 * Retrieve the timestamp from combined date and time fields
	 *
	 * @param  string $prefix  Prefix used to get request
	 * @param  array $request  Function request data
	 *
	 * @return integer         Generated timestamp
	 */
	private function build_timestamp( $prefix, $request ) {
		$str = sprintf(
			'%s %s',
			sanitize_text_field( $request['event-' . $prefix . '-date'] ),
			sanitize_text_field( $request['event-' . $prefix . '-date-time'] )
		);

		return strtotime( $str );
	}

}
