<?php

class WPI_Events_Taxonomy_Venue {

	/**
	 * Taxonomy name registered in Wordpress
	 */
	const NAME = 'venue';

	/**
	 * Singular taxonomy name
	 */
	const NAME_SINGULAR = 'Venue';

	/**
	 * Plural taxonomy name
	 */
	const NAME_PLURAL = 'Venues';

	/**
	 * Taxonomy rewrite slug
	 */
	const SLUG = 'venue';

	/**
	 * Init hooks & filters
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register' ] );
		add_action( self::SLUG . '_edit_form_fields', [ $this, 'edit_display_taxonomy_fields' ], 10, 2 );
		add_action( self::SLUG . '_add_form_fields', [ $this, 'add_display_taxonomy_fields' ], 10, 2 );
		add_action( 'edited_' . self::SLUG, [ $this, 'save_taxonomy_fields' ], 10, 2 );
		add_action( 'create_' . self::SLUG, [ $this, 'save_taxonomy_fields' ], 10, 2 );
	}

	/**
	 * Register taxonomy
	 */
	public function register() {
		$labels = array(
			'name'              => _x( self::NAME_PLURAL, 'taxonomy general name', WPI_EVENTS_LOCALE ),
			'singular_name'     => _x( self::NAME, 'taxonomy singular name', WPI_EVENTS_LOCALE ),
			'search_items'      => __( 'Search ' . self::NAME_PLURAL, WPI_EVENTS_LOCALE ),
			'all_items'         => __( 'All ' . self::NAME_PLURAL, WPI_EVENTS_LOCALE ),
			'parent_item'       => __( 'Parent ' . self::NAME, WPI_EVENTS_LOCALE ),
			'parent_item_colon' => __( 'Parent ' . self::NAME . ':', WPI_EVENTS_LOCALE ),
			'edit_item'         => __( 'Edit ' . self::NAME, WPI_EVENTS_LOCALE ),
			'update_item'       => __( 'Update ' . self::NAME, WPI_EVENTS_LOCALE ),
			'add_new_item'      => __( 'Add New ' . self::NAME, WPI_EVENTS_LOCALE ),
			'new_item_name'     => __( 'New ' . self::NAME . ' Name', WPI_EVENTS_LOCALE ),
			'menu_name'         => __( self::NAME_PLURAL, WPI_EVENTS_LOCALE )
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => self::SLUG )
		);

		register_taxonomy(
			self::SLUG,
			WPI_Events_Post_Type_Event::NAME,
			$args
		);
	}

	/**
	 * Render custom term fields on the edit page
	 *
	 * @param  WP_Term $term    WP_Term object
	 * @param  WP_Taxonomy      $taxonomy WP_Taxonomy object
	 */
	public function edit_display_taxonomy_fields( $term, $taxonomy ) {
		include WPI_EVENTS_ROOT . '/views/edit-taxonomy-fields-venue.php';
	}

	/**
	 * Render custom term fields on the add page
	 *
	 * @param  WP_Term $term    WP_Term object
	 * @param  WP_Taxonomy      $taxonomy WP_Taxonomy object
	 */
	public function add_display_taxonomy_fields( $term, $taxonomy ) {
		include WPI_EVENTS_ROOT . '/views/add-taxonomy-fields-venue.php';
	}

	/**
	 * Save custom term meta
	 *
	 * @param  integer $term_id Term ID
	 * @param  integer $tt_id   Term taxonomy ID
	 */
	public function save_taxonomy_fields( $term_id, $tt_id ) {
		if ( isset( $_POST[ 'address' ] ) ) {
			update_term_meta(
				$term_id,
				'address',
				sanitize_text_field( $_POST[ 'address' ] )
			);
		}
	}

}
