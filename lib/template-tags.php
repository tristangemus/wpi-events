<?php

/**
 * Display a minute selection field in 30 minute increments
 *
 * @param  string  $key      Field name
 * @param  boolean $required Required field or not
 */
function wpi_minute_field( $key, $required = true, $value ) {
	?>
	<select name="<?php echo $key; ?>" id="<?php echo $key; ?>"<?php echo $required ? ' required' : ''; ?>>
		<option value=""></option>
		<?php
		$start = strtotime( '00:00' );
		$end = strtotime( '24:00' );

		for ( $i = $start; $i < $end; $i += 30 * 60 ) {
			printf(
				'<option value="%1$s"%2$s>%1$s</option>',
				date( 'g:i A', $i ),
				selected( date( 'g:i A', $i ), $value, false )
			);
		}
		?>
	</select>
	<?php
}
