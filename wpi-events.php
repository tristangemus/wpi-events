<?php
/**
 * WPI Events
 *
 * @package     WPIEvents
 * @author      Tristan Gemus
 * @copyright   2018 WPInfantry
 * @license     GPL-2.0+
 *
 * @wpi-events
 * Plugin Name: WPI Events
 * Plugin URI:  https://www.wpinfantry.com
 * Description: Learning Wordpress development by building an event management plugin.
 * Version:     1.0.0
 * Author:      Tristan Gemus
 * Author URI:  https://www.wpinfantry.com
 * Text Domain: wpi-events
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

/**
 * Plugin constants
 */
define( 'WPI_EVENTS_ROOT', __DIR__ );
define( 'WPI_EVENTS_LOCALE', 'wpi-events' );
define( 'WPI_EVENTS_VERSION', '1.0.0' );

/**
 * Include main plugin files
 */
include WPI_EVENTS_ROOT . '/lib/class-post-type-event.php';
include WPI_EVENTS_ROOT . '/lib/class-taxonomy-venue.php';
include WPI_EVENTS_ROOT . '/lib/functions.php';
include WPI_EVENTS_ROOT . '/lib/template-tags.php';

/**
 * Instantiate plugin classes
 */
$GLOBALS['WPI_Events_Post_Type_Event'] = new WPI_Events_Post_Type_Event();
$GLOBALS['WPI_Events_Taxonomy_Venue'] = new WPI_Events_Taxonomy_Venue();

/**
 * Enqueue admin-only scripts
 */
function wpi_events_admin_scripts() {
	wp_enqueue_script( 'jquery-ui-datepicker' );
	wp_enqueue_script( 'wpi-events-admin', plugins_url( 'inc/js/admin.js', __FILE__ ), ['jquery', 'jquery-ui-datepicker'], WPI_EVENTS_VERSION, true );
	wp_enqueue_style( 'jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' );
}
add_action( 'admin_enqueue_scripts', 'wpi_events_admin_scripts' );
