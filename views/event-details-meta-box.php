<table>
	<tr>
		<td width="100">
			<label for="event-start-date">Start Date</label>
		</td>
		<td width="100">
			<input type="text" id="event-start-date" name="event-start-date" class="datepicker" value="<?php
				echo $start_date['date']
			?>" required />
		</td>
		<td width="30">
			<?php wpi_minute_field( 'event-start-date-time', true, $start_date['time'] ); ?>
		</td>
	</tr>
	<tr>
		<td width="100">
			<label for="event-end-date">End Date</label>
		</td>
		<td width="100">
			<input type="text" id="event-end-date" name="event-end-date" class="datepicker" value="<?php
				echo $end_date['date']
			?>" />
		</td>
		<td width="30">
			<?php wpi_minute_field( 'event-end-date-time', true, $end_date['time'] ); ?>
		</td>
	</tr>
</table>
