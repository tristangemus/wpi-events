<tr class="form-field">
	<th scope="row" valign="top">
		<label for="address"><?php _e( 'Address', WPI_EVENTS_LOCALE ); ?></label>
	</th>
	<td>
		<input type="text" name="address" value="<?php
			echo get_term_meta( $term->term_id, 'address', true );
		?>" />
	</td>
</tr>
