<?php get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
				$has_thumbnail = has_post_thumbnail();
			?>
				<div class="single-inline-event<?php echo $has_thumbnail ? ' has-thumbnail' : ''; ?>">
					<div class="image">
						<?php if ( $has_thumbnail ) {
							printf(
								'<a href="%s">%s</a>',
								get_permalink(),
								get_the_post_thumbnail( null, 'medium' )
							);
						} ?>
					</div>
					<div class="event-summary">
						<h3><?php the_title(
							'<a href="' . get_permalink() . '">',
							'</a>'
						); ?></h3>
						<p class="inline-date"><?php wpi_event_date( get_the_ID() ); ?></p>
						<?php the_content(); ?>
					</div>
				</div>
			<?php endwhile; endif; ?>
		</main>
	</div>
</div>
<?php get_footer(); ?>
