<div class="form-field term-slug-wrap">
	<label for="address"><?php _e( 'Address', WPI_EVENTS_LOCALE ); ?></label>
	<input type="text" name="address" value="<?php
		echo get_term_meta( $term->term_id, 'address', true );
	?>" />
</div>
